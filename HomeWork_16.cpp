﻿#include <iostream>

const int N = 3;
const int calendar = 22;

void Print(int array[N][N]) {

    for (int i = 0; i < N; i++) {

        for (int j = 0; j < N; j++) {

            std::cout << array[i][j];

        }

        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Sum(int array[N][N]) {

    int summa = 0;

    for (int i = 0; i < N; i++) {


        if ((calendar % N) == i) {

            for (int j = 0; j < N; j++) {

                summa += array[i][j];

            }

            std::cout << summa;
        }
    }
}

int main()
{
    int array[N][N];

    for (int i = 0; i < N; i++) {

        for (int j = 0; j < N; j++) {

            array[i][j] = i + j;
        }
    }

    Print(array);
    Sum(array);

    return 0;
}

